# 基于CNN的手势识别

#### 一、运行教程
1. 下载数据集，数据集下载网址[gesture](https://www.idiap.ch/webarchives/sites/www.idiap.ch/resource/gestures/data/)，注意下载**shp_marcel_test.tar.gz**和**shp_marcel_train.tar.gz**两个压缩文件，分别解压后放到工程目录中的/data文件夹中
2. 运行preprocess.py数据预处理文件，运行完成后在data目录下会生成四个文件（三个txt和一个json）
3. 将运行环境修改为GPU（config.py文件中的device变量修改为cuda，没有安装cuda跳过该步骤）
4. 在工程目录中创建/log和/model_save文件夹
5. 运行train.py训练脚本
6. 监控训练情况，打开命令行，输入tensorboard --logdir={工程目录中log文件夹的绝对路径} 

   例如
   ```shell
   tensorboard --logdir=C:\desktop\cnn-gesture\log
   ```
   浏览器访问6006端口:[训练情况监控](http://localhost:6006)
7. 修改config.py文件中device变量的值为cpu
8. 训练完成后根据Loss曲线选择最优模型，在model_save文件夹中选择靠后的模型文件即可
9. 修改inference.py文件中的第九行，将选择好的pth文件路径替换即可
10. 运行inference.py文件，输出准确率即运行成功

### 二、网络结构
图像识别网络的整体架构如下图所示

<img src="assets/all.png" alt="">

每个MTB块的基本结构如图所示

<img src="assets/MTB.png" alt="">
    